# Add your targets (i.e. the name of your output program) here
BINARIES = pixy_driver pixy_example pixy_example_threaded

BINARIES := $(addprefix bin/,$(BINARIES))

CC = gcc
CFLAGS = -g -Wall -std=gnu99 `pkg-config --cflags lcm`
LDFLAGS = `pkg-config --libs lcm`


.PHONY: all clean lcmtypes bbblib

all: lcmtypes bbblib $(BINARIES)

lcmtypes:
	@$(MAKE) -C lcmtypes

bbblib:
	@$(MAKE) -C bbblib

clean:
	@$(MAKE) -C lcmtypes clean
	@$(MAKE) -C bbblib clean
	rm -f *~ *.o bin/*

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

lcmtypes/%.o: lcmtypes/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

bin/pixy_driver: pixy_driver.o util.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example: pixy_example.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example_threaded: pixy_example_threaded.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

# Add build commands for your targets here
